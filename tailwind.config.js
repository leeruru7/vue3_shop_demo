module.exports = {
    important: false,
    content: [
        './src/**/*.html',
        './src/**/*.vue',
    ],
    theme: {
        extend: {
            colors: {
                'nl-dark': {
                    'gray-purple': '#312536'
                },
                'nl-brown': {
                    '100': '#6a3528',
                    '200': '#2d110a'
                },
                'nl-blue': {
                    '100': '#2f3b4d',
                    '130': '#375890',
                    '150': '#304d85',
                    '200': '#202f44',
                },
                'nl-bright-blue': '#00479d',
                'nl-red': {
                    '50': '#ffc2c9',
                    '70': '#ffadad',
                    '100': '#ff9090',
                    '200': '#ff8080',
                    '300': '#e45465'
                },
                'nl-purple': {
                    '100': '#b36889',
                    '200': '#a04e72',
                },
                'nl-teal': {
                    '10': '#93c1c6',
                    '50': '#76cbd0',
                    '100': '#54a2a9',
                    '200': '#1c6f77',
                },
                'nl-pink': {
                    '100': '#ffafd3',
                    '200': '#e45465'
                },
                'nl-bright-green': {
                    '100': '#44cbc0',
                    '200': '#13b5b1',
                },
                'nl-green': {
                    '500': '#074f21'
                },
                'nl-yellow': {
                    '200': '#fff45c',
                    '300': '#fff025'
                }

            }
        }
    },
    variants: {},
    plugins: [],
}
