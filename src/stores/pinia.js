import { defineStore } from "pinia";

export const useStore = defineStore('store', {
    state() {
        return {
            isEncryption: false,
            showLog: import.meta.env.VITE_APP_LOG === 'true',
            isLoading: false,
            pagination: {},
            cartTotal: 0,
            order: [],
            orderLength: 0,
            isLogin: !!sessionStorage.getItem('isLogin'),
            memberData: sessionStorage.getItem('memberData'),
            product: [],
            favorites: [],
            carts: [],
            baseUrl: import.meta.env.VITE_APP_BASE_URL,
            apiUrl: import.meta.env.VITE_APP_API_URL,
            cartSwitch: false,
            showOrder: false,
        }
    },
    // 對應 computed
    getters: {},
    // 對應 methods
    actions: {
        setIsLogin(status) {
            this.isLogin = status
            sessionStorage.setItem('isLogin', status)
        },
        clearIsLogin() {
            this.isLogin = false
            sessionStorage.removeItem('isLogin')
        },
        setMemberData(data) {
            this.memberData = data
            sessionStorage.setItem('memberData', data)
            this.favorites = JSON.parse(data).favorites || []
            this.carts = JSON.parse(data).carts || []
            this.cartTotal = JSON.parse(data).carts_total || 0
            this.order = JSON.parse(data).order || []
        },
        clearMemberData() {
            this.memberData = []
            this.favorites = []
            this.carts = []
            this.cartTotal = 0
            this.order = []
            sessionStorage.removeItem('memberData')
        },
        setFavorite(data) {
            this.favorites = JSON.parse(data);
        },
        setCartSwitch(val) {
            this.cartSwitch = val
        },
        setCart(data) {
            if (data) {
                this.carts = JSON.parse(data);
            } else {
                this.carts = [];
            }
        },
        setTotal(data) {
            this.cartTotal = data;
        },
        setOrder(data) {
            this.order = JSON.parse(data);
        },
        setShowOrder(val) {
            this.showOrder = val;
        }
    },
    persist: {
        enabled: true
    }
})


export const useStoreAlert = defineStore('alert', {
    state() {
        return {
            messages: []
        }
    },
    actions: {
        updateMessage (message, status) {
            const timestamp = Math.floor(new Date() / 10);
            const newMessage = {message, status, timestamp};
            this.messages.push(newMessage);
            // 依序刪除
            this.removeMessageWithTiming(timestamp);
        },
        removeMessageWithTiming(timestamp) {
            const msg = this.messages;
            setTimeout(() => {
                msg.forEach(item => {
                  if (item.timestamp === timestamp) {
                    this.removeMessage(timestamp);
                  }
                });
              }, 1500);
        },
        removeMessage(timestamp) {
            this.messages = this.messages.filter(item => {
                return item.timestamp !== timestamp;
            })
        }
    }
})
