import {createRouter, createWebHistory} from "vue-router";

const routes = [
    {
        path: "/",
        name: "index",
        component: () => import("../views/index.vue"),
    },
    {
        path: "/member",
        name: "member",
        component: () => import("../views/Member.vue"),
    },
    {
        path: "/productList",
        name: "productList",
        component: () => import("../views/ProductList.vue"),
    },
    {
        path: '/productList/:productId',
        name: 'Product',
        component: () => import('../views/ProductInfo.vue'),
    },
    {
        path: "/checkout",
        name: "checkout",
        component: () => import("../views/Checkout.vue"),
    },
]
export default createRouter({
    history: createWebHistory(),
    routes,
    scrollBehavior(to, from, savedPosition) {
        // always scroll to top
        return { top: 0 }
    },
});
