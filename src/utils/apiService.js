
// apiService.js
import { useStore } from '../stores/pinia'
import axios from 'axios'
// api
import api from '../api'

export const apiService = {
    install(app) {
        app.config.globalProperties.$apiService = new api(axios, useStore())
        app.provide('$api', app.config.globalProperties.$apiService);
    }
}
