export function parseTime(time) {
    const date = new Date(time);
    const y = date.getFullYear();
    const m = (date.getMonth() + 1 < 10 ? '0' : '') + (date.getMonth() + 1);
    const d = (date.getDate() < 10 ? '0' : '') + date.getDate();
    const h = (date.getHours() < 10 ? '0' : '') + date.getHours();
    const min = (date.getMinutes() < 10 ? '0' : '') + date.getMinutes();
    return y + '-' + m + '-' + d + ' ' + h + ':' + min;
}
