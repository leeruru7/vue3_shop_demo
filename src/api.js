import CryptoJS from 'crypto-js';

export default class api {
    constructor(axios, useStore) {
        this._axios = axios;
        this._store = useStore;
        this._aesKey = import.meta.env.VITE_APP_API_KEY;
        this._iv = '0000000000000000';
    }

    setStaticFail(value) {
        this._staticFail = value;
    }

    setStaticError(value) {
        this._staticError = value;
    }

    encryptedData(body) {
        const key = CryptoJS.enc.Utf8.parse(this._aesKey);
        const ivKey = CryptoJS.enc.Utf8.parse(this._iv);
        if (body instanceof Object) {
            body = JSON.stringify(body)
        }

        return CryptoJS.AES.encrypt(body, key, {
            iv: ivKey,
            mode: CryptoJS.mode.CBC,
            padding: CryptoJS.pad.Pkcs7
        }).toString();
    }

    decryptedData(body) {
        const key = CryptoJS.enc.Utf8.parse(this._aesKey);
        const ivKey = CryptoJS.enc.Utf8.parse(this._iv);
        const decrypt = CryptoJS.AES.decrypt(body, key, {
            iv: ivKey,
            mode: CryptoJS.mode.CBC,
            padding: CryptoJS.pad.Pkcs7
        });

        return CryptoJS.enc.Utf8.stringify(decrypt).toString();
    }

    hashData(value) {
        return CryptoJS.SHA256(value).toString();
    }

    getApiUrl() {
        return this._store.apiUrl;
    }

    id() {
        return JSON.parse(this._store.memberData).id;
    }

    // 註冊
    register(account, password, email) {
        const api = this.getApiUrl() + '/memberList'
        const data = {
            account: account,
            password: password,
            name: '',
            birthday: '',
            phone: '',
            email: email,
            address: '',
            favorites: ''
        }
        return this.post('register', data, api)
    }

    // 取得會員列表
    getMemberList() {
        const api = this.getApiUrl() + '/memberList';
        return this.get('getMemberList', api);
    }

    // 取得產品列表
    getProductList() {
        const api = this.getApiUrl() + '/productList';
        return this.get('getProductList', api);
    }

    // 更新會員資料
    updateMember(memberData) {
        const api = this.getApiUrl() + '/memberList/' + memberData.id
        const data = {
            name: memberData.name,
            birthday: memberData.birthday,
            phone: memberData.phone,
            email: memberData.email,
            address: memberData.address
        }
        return this.patch('updateMember', data, api)
    }

    // 更新我的最愛
    updateFavorite(favorites) {
        const api = this.getApiUrl() + '/memberList/' + this.id()
        const data = {
            favorites: favorites,
        }
        return this.patch('updateFavorite', data, api)
    }

    // 更新購物車
    updateCart(cartsList, total) {
        const api = this.getApiUrl() + '/memberList/' + this.id()
        const data = {
            carts: cartsList,
            carts_total: total
        }
        return this.patch('updateCart', data, api)
    }

    // 新增訂單
    updateOrder(order) {
        const api = this.getApiUrl() + '/memberList/' + this.id()
        const data = {
            order: order
        }
        return this.patch('updateOrder', data, api)
    }

    post(command, data = {}, api, timeout) {
        let body = '';
        // if (this._store.isEncryption) {
        //     body = {company_id: this._store.company_id, data: this.encryptedData(data)};
        // } else {
        //     body = {company_id: this._store.company_id, data: data};
        // }
        body = data;

        if (this._store.showLog) {
            console.log('request command = ' + command);
            console.log(data);
        }

        return new Promise((resolve, reject) => {

            // this._axios.post(api, JSON.stringify(body), {timeout: timeout}).then(res => {
            this._axios.post(api, body, {timeout: timeout}).then(res => {
                if (this._store.showLog) {
                    console.log('$http post 成功 回傳如下');
                    console.log('response command = ' + command);
                    console.log(res);
                }

                let resData = '';
                if (res.status >= 200 && res.status < 210) {
                    // if (res.data.error_code === 0) {
                    //     if (res.data.data !== null) {
                    //         resData = this._store.isEncryption ? JSON.parse(this.decryptedData(res.data.data)) : res.data.data;
                    //     } else {
                    //         resData = '';
                    //     }
                    resData = res.data;
                    resolve(resData);
                } else {
                    if (this._staticFail) {
                        this._staticFail(res.data.error_code);
                    }
                    reject(res.data);
                }

            }).catch(error => {
                if (this._store.showLog) {
                    console.log('$http post 失敗 回傳如下');
                    console.log('command = ' + command);
                    console.log(error);
                }
                if (this._staticError) {
                    this._staticError(error, command);
                }
            })
        });
    }

    get(command, api, timeout) {

        return new Promise((resolve, reject) => {

            this._axios.get(api, {timeout: timeout}).then(res => {
                if (this._store.showLog) {
                    console.log('response command = ' + command);
                    console.log(res);
                }
                let resData = '';
                if (res.status === 200) {
                    if (res.data !== null) {
                        resData = res.data;
                    } else {
                        resData = '';
                    }
                    resolve(resData);
                } else {
                    if (this._staticFail) {
                        this._staticFail(res.status);
                    }
                    reject(res.data);
                }

            }).catch(error => {
                if (this._store.showLog) {
                    console.log('$http post 失敗 回傳如下');
                    console.log('command = ' + command);
                    console.log(error);
                }
                if (this._staticError) {
                    this._staticError(error, command);
                }
            })
        });
    }

    patch(command, data = {}, api, timeout) {
        let body = '';
        body = data;

        if (this._store.showLog) {
            console.log('request command = ' + command);
            console.log(data);
        }

        return new Promise((resolve, reject) => {

            // this._axios.post(api, JSON.stringify(body), {timeout: timeout}).then(res => {
            this._axios.patch(api, body, {timeout: timeout}).then(res => {
                if (this._store.showLog) {
                    console.log('$http post 成功 回傳如下');
                    console.log('response command = ' + command);
                    console.log(res);
                }

                let resData = '';
                if (res.status >= 200 && res.status < 210) {
                    // if (res.data.error_code === 0) {
                    //     if (res.data.data !== null) {
                    //         resData = this._store.isEncryption ? JSON.parse(this.decryptedData(res.data.data)) : res.data.data;
                    //     } else {
                    //         resData = '';
                    //     }
                    resData = res.data;
                    resolve(resData);
                } else {
                    if (this._staticFail) {
                        this._staticFail(res.data.error_code);
                    }
                    reject(res.data);
                }

            }).catch(error => {
                if (this._store.showLog) {
                    console.log('$http patch 失敗 回傳如下');
                    console.log('command = ' + command);
                    console.log(error);
                }
                if (this._staticError) {
                    this._staticError(error, command);
                }
            })
        });
    }

}
