import {createApp} from 'vue'
import { createMetaManager } from 'vue-meta'
import {createPinia} from 'pinia'
import piniaPersist from 'pinia-plugin-persist'
import App from './App.vue'
import router from './router/index'
import axios from 'axios'
import VueAxios from 'vue-axios'
import { apiService } from './utils/apiService.js'

// pinia
const pinia = createPinia();
pinia.use(piniaPersist)

// tailwind css
import './assets/css/style.css'


createApp(App)
    .use(VueAxios, axios)
    .use(createMetaManager())
    .use(pinia)
    .use(apiService)
    .use(router)
    .mount('#app')
